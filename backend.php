<?php
    session_start();

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: origin, content-type, accept"); 

    include_once 'connector.php'; 
    include_once 'config.php'; 
    include_once 'tables/routes.php'; 
    include_once 'tables/seed.php';


    if(Config::onLocalhost()){
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(-1);


        $conf = Config::getConfigs(); 
        if($conf['seed_db']){
            seed(); 
            Config::ImDoneWithSeed(); 
        }
    }

    //Main logic for routing
    if(isset($_GET['action'])){

        if(function_exists($_GET['action'])){
            if(authenticate($_GET['action'])){
                call_user_func($_GET['action']); 
            }else{
                echo "Unauthorized"; 
                http_response_code(401);
            }
        }else{
            echo "Route is not definied";
            http_response_code(400);
        }
    }

    function formatToJSON($output){
        
        foreach($output as $key => &$value) { 
            foreach($value as $key => &$value2) {
                $value2 = utf8_encode($value2); 
            }
        }
    
        return json_encode($output);
    }

    
    //Perform authentication of users
    function authenticate($action){

        $routes = Config::getRoutes(); 

        if(array_key_exists($action, $routes)){
            if($routes[$action] === 0){
                return true; 
            }else{

                //perform auth
                if(isset($_SESSION['user'])){
                    return true; 
                }else{
                    return false; 
                }
            }
            
        }else{
            echo "action is not in route"; 
            http_response_code(405);
            return false; 
        }
    }

    function getJsonInput(){
        $json = file_get_contents('php://input');
        return json_decode($json);
    }

    function loginUser(){
        $user = getJsonInput();
        $responsTxt = ""; 
        $responsCode = 0; 

        $dbUser = getUser($user->username); 

        if(sha1($user->password) == $dbUser['password']){
            $responsTxt = "Du är nu inloggad";
            $responsCode = 1;
            $_SESSION['user'] = $dbUser['username']; 
        }else{
            $responsTxt =  "Du angav inte rätt användarnamn eller lösenord"; 
        }

        $respons = array(
            'responsTxt' => $responsTxt,
            'responsCode' => $responsCode
        );

        echo json_encode($respons);  
    }

    function getUserStatus(){
        
        if(isset($_SESSION['user']))
        {
                
                $user = array(
                    'status' => 'Logga ut',   
                    'username' => $_SESSION['user'],
                    'statusCode' => 0
                ); 
                echo json_encode($user); 
                
        }else{
            
            $user = array(
                'status' => 'Logga in',   
                'username' => '[inte inloggad]',
                'statusCode' => 1
            ); 

            echo json_encode($user); 
            
        }
        

    }

    function logoutUser(){
        $_SESSION['user'] = ''; 

        $user = array(
            'status' => 'Logga in',   
            'username' => '[inte inloggad]',
            'statusCode' => 1
        ); 

        echo json_encode($user); 
    }

    function ping(){
        echo "pong"; 
    }

?>