<?php
class Config{

    public static function getConfigs(){
        $string = file_get_contents("config.json");
        return json_decode($string, true);
    }

    public static function ImDoneWithSeed(){
        $conf = self::getConfigs(); 
        $conf['seed_db'] = 0; 

        $newJsonData = json_encode($conf, JSON_PRETTY_PRINT);
        file_put_contents('config.json', $newJsonData);
    }

	public static function getRoutes(){
        $routes = array(
        	'seed' => 0, 
            'loginUser' => 0,
            'logoutUser' => 0,
            'createUser' => 1,
            'ping' => 0,
            'getUser' => 0,
            'getColors' => 0
        ); 

        return $routes; 
	}

	public static function onLocalhost(){
		if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
		    return true; 
		}else{
			return false; 
		}
	}
}
?>