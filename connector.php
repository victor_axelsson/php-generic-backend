<?php

    function GetConnecor(){
        if(Config::onLocalhost()){
            $conf = Config::getConfigs(); 
            return mysqli_connect($conf['database']['localhost']['host'], $conf['database']['localhost']['user'], $conf['database']['localhost']['password'], $conf['database']['localhost']['db_name']);
        }else{
            return mysqli_connect($conf['database']['live']['host'], $conf['database']['live']['user'], $conf['database']['live']['password'], $conf['database']['live']['db_name']);
        }
    }

    function execute($cmd){
        $con = GetConnecor();
        mysqli_set_charset($con, "ISO-8859-1"); 
        
        $result = mysqli_query($con, $cmd);
        
        if ($result === false) {
            printf("error: %s\n", mysqli_error($con));
        }
        
        mysqli_close($con);
        return $result;
    }
?>