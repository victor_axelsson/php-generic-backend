<?php

include_once 'table.php'; 

function seed(){
	$tables = Config::getConfigs()['tables']; 

	foreach($tables as $table){
		echo $table['name'] ."<br>"; 	

		$schema = array(); 
		
		foreach ($table['columns'] as $column) {
			foreach ($column as $key => $value) {
				$schema[$key] = $value; 
			}
		}	
		$sqlCreate = buildCreateTable($table['name'], $schema); 
		execsql($sqlCreate, null, null, true);

		//seed		
		if(array_key_exists('seed', $table)){
			$seeds = array(); 
			foreach ($table['seed'] as $seed) {
				foreach ($seed as $key => $value) {
					$seeds[$key] = $value; 
				}

				$sql = "INSERT INTO " .$table['name'] . " ( ";
				foreach ($seeds as $key => $value) {
				 	$sql .= $key .",";  
				} 
				$sql = rtrim($sql, ","); 
				$sql .= " ) VALUES ( "; 
				foreach ($seeds as $key => $value) {
				 	$sql .= " ?,";  
				} 
				$sql = rtrim($sql, ","); 
				$sql .= ")"; 

				execsql($sql, $seeds, null, true);
			}
		}
	}
}

function buildCreateTable($tablename, $parameters){
	$sql = 	"CREATE TABLE IF NOT EXISTS " .$tablename . " ( ";
	
	foreach ($parameters as $key => $value) {
		$sql .= $key ." " .$value .","; 
	}
	$sql = rtrim($sql, ","); 
	$sql .= " )"; 
	return $sql;  
}



?>