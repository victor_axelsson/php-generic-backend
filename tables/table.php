<?php

function paramtypez($val){
    $types = '';                        //initial sting with types
    foreach($val as $para) {     
        if(is_int($para)) {
            $types .= 'i';              //integer
        } elseif (is_float($para)) {
            $types .= 'd';              //double
        } elseif (is_string($para)) {
            $types .= 's';              //string
        } else {
            $types .= 'b';              //blob and unknown
        }
    }
    return $types;
}

function execsql($query, $param, $result, $private = false){
    $conn = GetConnecor();

    if($row=$conn->prepare($query))
    {
        if(!is_null($param)){
        	array_unshift($param, paramtypez($param));
        	call_user_func_array(array($row, 'bind_param'), makeValuesReferenced($param));
        }

        $row->execute();

        if(isset($result)){
        	call_user_func_array(array($row, 'bind_result'), makeValuesReferenced($result)); 
		}
		
        $output = array(); 
         while ($row->fetch()) {

         	$outputCol = null; 
         	foreach ($result as $key => $val) {
	         		$outputCol[$key] = $val;  
         	}
         	array_push($output, $outputCol); 
         }
         
         if($private){
         	return $output; 
         }else{
         	
         	echo json_encode($output);  
         }

        return true;
    }
    else{
    	http_response_code(400);
  	  	return false;
    }

}

function makeValuesReferenced(&$arr){
    $refs = array();
    foreach($arr as $key => $value)
        $refs[$key] = &$arr[$key];
    return $refs;
}

?>